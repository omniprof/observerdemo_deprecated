package com.kenfogel.observerdemo_deprecated;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Demo class that uses the Observer/Observable pattern as implemented by Java
 * to notify tow other frames that the text in this frame has changed
 *
 * @author neon
 *
 */
@SuppressWarnings("serial")
public class ObserverDemo extends JFrame {

    private JLabel label;
    private JTextField textField;
    private ObservedData observedData;
    private ObservedViewOne ovo;
    private ObservedViewTwo ovt;

    /**
     * Constructor that creates a frame with a text field in it and then
     * registers the ObeservedData so that when this frame changes the data then
     * the other two frames are notified
     */
    public ObserverDemo() {
        super("Observable Demo");
        initialize();
    }

    /**
     * Create the GUI and add the two View classes as listeners
     */
    private void initialize() {
        Container c = getContentPane();
        c.setLayout(new FlowLayout());
        label = new JLabel("Enter any text:");
        textField = new JTextField(10);
        textField.addActionListener(new MyTextListener());
        c.add(label);
        c.add(textField);

        // Instantiate the data
        observedData = new ObservedData();

        // Instantiate the other two frames
        ovo = new ObservedViewOne();
        ovt = new ObservedViewTwo();

        // Register the frames with the ObservedData
        observedData.addObserver(ovo);
        observedData.addObserver(ovt);

        // Provide an initial value
        observedData.setObservedData("Initial Text");

        setSize(275, 80);
        setResizable(false);
        centerScreen();
        setVisible(true);
    }

    /**
     * Center this frame on the display
     */
    private void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                (dim.height - abounds.height) / 2);
    }

    /**
     * The inner class that acts as the listener for the text field
     *
     * @author neon
     *
     */
    class MyTextListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            observedData.setObservedData(textField.getText());
        }
    }

    /**
     * The main method
     *
     * @param args
     */
    public static void main(String args[]) {
        ObserverDemo od = new ObserverDemo();
        od.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
