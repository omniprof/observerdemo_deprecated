package com.kenfogel.observerdemo_deprecated;

import java.awt.*;
import javax.swing.*;
import java.util.*;

/**
 * A Frame class that is called whenever the model changes
 * @author neon
 *
 */
@SuppressWarnings("serial")
public class ObservedViewOne extends JFrame implements Observer {

	private JLabel label;

	/**
	 * Constructor that sets up the frame.
	 */
	public ObservedViewOne() {
		super("Observed View One");

		// Retrieve the content pane (panel) of the frame
		// Used when placing items directly in the frame
		Container c = getContentPane();
		c.setLayout(new FlowLayout());
		// Set the initial value of the label
		label = new JLabel("Text to Come!");

		c.add(label);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(150, 80);
		setResizable(false);
		upperLeftScreen();
		setVisible(true);
	}

	/**
	 * Place the frame in the upper left of the screen
	 */
	private void upperLeftScreen() {
		setLocation(1, 2);
	}

	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 * Callback method for the observable event
	 */
	public void update(Observable observable, Object object) {
		
		// Determine the source of the observer event
		if (observable instanceof ObservedData) {
			ObservedData observedData = (ObservedData) observable;
			label.setText(observedData.getTheObservedData());
			//object is null if not used
			//label.setText((String)object);
		}
	}
}
