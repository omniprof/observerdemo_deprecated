package com.kenfogel.observerdemo_deprecated;

import java.util.Observable;

/**
 * Sample class that represents a model that needs to
 * inform other objects when it is changed
 * @author neon
 *
 */
public class ObservedData extends Observable {
	private String theObservedData;

	/**
	 * Set the data as called from another object
	 * @param theData
	 */
	public void setObservedData(String theData) {
		theObservedData = theData;
		// must call setChanged before notifyObservers to
		// indicate model has changed
		setChanged();

		// Notify Observers that model has changed
		// The observers will receive a reference to this object so
		// that they may retrieve the new data
		notifyObservers();
		// Overloaded version that allows data, such as the new data,
		// to be sent in the notification. Including the changed data
		// in the notification means that the observers do not have to
		// call back to the data object
		//notifyObservers(theObservedData);
	}

	/**
	 * Get the data
	 * @return the observed data
	 */
	public String getTheObservedData() {
		return theObservedData;
	}
}
