package com.kenfogel.observerdemo_deprecated;

import java.awt.*;

import javax.swing.*;

import java.util.*;

/**
 * A Frame class that is called whenever the model changes
 * @author neon
 *
 */@SuppressWarnings("serial")
public class ObservedViewTwo extends JFrame implements Observer {

	private JLabel label;

	/**
	 * Constructor that sets up the frame.
	 */
	public ObservedViewTwo() {
		super("Observed View Two");
		// Retrieve the content pane (panel) of the frame
		// Used when placing items directly in the frame
		Container c = getContentPane();
		c.setLayout(new FlowLayout());
		// Set the initial value of the label
		label = new JLabel("Text to Come!");

		c.add(label);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(150, 80);
		setResizable(false);
		lowerRightScreen();
		setVisible(true);
	}

	/**
	 * Place the frame in the lower right of the screen as
	 * calculated based on the screen dimensions
	 */
	private void lowerRightScreen() {
		Dimension dim = getToolkit().getScreenSize();
		Rectangle abounds = getBounds();
		setLocation((dim.width - abounds.width),
				(dim.height - abounds.height - 80));
	}

	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 * Callback method for the observable event
	 */
	public void update(Observable observable, Object object) {
		
		// Determine the source of the observer event
		if (observable instanceof ObservedData) {
			ObservedData observedData = (ObservedData) observable;
			label.setText(observedData.getTheObservedData());
			//object is null if not used
			//label.setText((String)object);
		}
	}
}
